// variable "comorb_data_schema" contains data schema for comorbidity-view network data

var comorb_data_schema = {
	nodes : [{
		name : "label",
		type : "string"
	}, {
		name : "swe_description",
		type : "string"
	}, {
		name : "eng_description",
		type : "string"
	}, {
		name : "frequency",
		type : "int"
	}, {
		name : "mappervalue",
		type : "int"
	}],
	edges : [{
		name : "source_code",
		type : "string"
	}, {
		name : "target_code",
		type : "string"
	}, {
		name : "frequency",
		type : "int"
	}, {
		name : "source_percentage",
		type : "int"
	}, {
		name : "target_percentage",
		type : "int"
	}, {
		name : "mappervalue",
		type : "int"
	}]
}