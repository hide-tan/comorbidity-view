<?php
// check if the data are sent by post method with variable "dcode"
if (isset($_POST["dcode"])) {
	// set path to the code list. each row of the code list contains "dcode", "gender", "byear", "threshold"
	$clistname = "/usr/local/bib/networkdata/codelist";
	if (file_exists($clistname)) {
		$clist = fopen($clistname, "r");
		$found = false; // boolean value if a network data for the sent diagnosis code exist or not
		$hit = ""; // return value
		$id_count = 0; // id nr
		
		while (!feof($clist)) {
			$row = fgetcsv($clist); // split csv data into an array "row"
			// check if the diagnosis code of the current line equals to the sent diagnosis code
			if (strnatcasecmp($_POST["dcode"], $row[0]) == 0) {
				// if matches, append the gender, byear and threshold info to the return value
				$id_count++;
				$hit .= '<tr id="result_id_'.$id_count.'"><td align="center">' . $row[1] . '</td><td align="center">' . $row[2] . '</td><td align="center">' . $row[3] . '</td><td><input type="button" value="Show view" class="network_from_table" name='.$row[1].'#'.$row[2].'#'.$row[3]. '></td></tr>';
				// changes boolean value
				if ($found == false)
					$found = true;
			}
		}
		
		// if network data for the sent diagnosis code are missing, print an error message
		if ($found == false) {
			print 'No network data found for ' . $_POST["dcode"];
		} 
		// otherwise create an html code (table) containing the gender, byear and threshold info
		else {
			$result = '<table cellpadding="5"><tr><td class="result_header">Gender</td><td class="result_header">Birth year</td><td class="result_header">Threshold</td><td></td></tr>' . $hit . '</table>';
			print $result;
		}
		// closes the code list
		@fclose($clistname);
	}
}
?>
