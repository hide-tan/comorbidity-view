// id of Cytoscape Web container div
var div_id = "cytoscapeweb";

// initialization options
var options = {
	swfPath : "swf/CytoscapeWeb",
	flashInstallerPath : "swf/playerProductInstall"
};
var default_layout_options = {
		layoutQuality : "default",
		incremental : true,
		tension : 0,
		restLength : 100,
		smartRestLength : true,
		gravitation : -1000,
		smartDistance : true,
		centralGravitation : 100,
		centralGravityDistance : 50,
		compoundCentralGravitation : 120,
		compoundCentralGravityDistance : 40,
		multiLevelScaling : true,
		uniformLeafNodeSizes : false
};

var visual_style_data = {
	global : {
		backgroundColor : "#ffffff"
	},
	nodes : {
		shape : "ROUNDRECT",
		compoundShape : "ROUNDRECT",
		borderWidth : 1,
		borderColor : "#212121",
		//selectionBorderWidth : 1,
		selectionBorderColor : "#000000",
		selectionGlowColor : "#000000",
		selectionGlowOpacity :1, //0 to 1 default 0.6
		selectionGlowBlur : 32, // 0 to 255 default 8
		selectionGlowStrength : 15, // 0 to 255 default 6
		size : {
			 passthroughMapper: { attrName: "mappervalue" } 
		},
		color : {
			discreteMapper : {
				attrName : "parent",
				entries : [{
					attrValue : 1,
					value : "#ed145b"
				}, {
					attrValue : 2,
					value : "#ec008c"
				}, {
					attrValue : 3,
					value : "#92278f"
				}, {
					attrValue : 4,
					value : "#662d91"
				}, {
					attrValue : 5,
					value : "#2e3192"
				}, {
					attrValue : 6,
					value : "#0054a6"
				}, {
					attrValue : 7,
					value : "#0072bc"
				}, {
					attrValue : 8,
					value : "#00aeef"
				}, {
					attrValue : 9,
					value : "#00a99d"
				}, {
					attrValue : 10,
					value : "#00a651"
				}, {
					attrValue : 11,
					value : "#39b54a"
				}, {
					attrValue : 12,
					value : "#8dc63f"
				}, {
					attrValue : 13,
					value : "#fff200"
				}, {
					attrValue : 14,
					value : "#f7941d"
				}, {
					attrValue : 15,
					value : "#f26522"
				}, {
					attrValue : 16,
					value : "#ed1c24"
				}, {
					attrValue : 17,
					value : "#736357"
				}, {
					attrValue : 18,
					value : "#8c6239"
				}, {
					attrValue : 19,
					value : "#534741"
				}, {
					attrValue : 20,
					value : "#754c24"
				}, {
					attrValue : 21,
					value : "#9a9a9a"
				}, {
					attrValue : 22,
					value : "#818181"
				}]
			}
		},
		compoundColor : {
			discreteMapper : {
				attrName : "id",
				entries : [{
					attrValue : 1,
					value : "#f5989d"
				}, {
					attrValue : 2,
					value : "#f49ac1"
				}, {
					attrValue : 3,
					value : "#bd8cbf"
				}, {
					attrValue : 4,
					value : "#a186be"
				}, {
					attrValue : 5,
					value : "#8781bd"
				}, {
					attrValue : 6,
					value : "#8393ca"
				}, {
					attrValue : 7,
					value : "#7da7d9"
				}, {
					attrValue : 8,
					value : "#6dcff6"
				}, {
					attrValue : 9,
					value : "#7accce"
				}, {
					attrValue : 10,
					value : "#82ca9c"
				}, {
					attrValue : 11,
					value : "#a3d39c"
				}, {
					attrValue : 12,
					value : "#c4df9b"
				}, {
					attrValue : 13,
					value : "#fff799"
				}, {
					attrValue : 14,
					value : "#fdc689"
				}, {
					attrValue : 15,
					value : "#f9ad81"
				}, {
					attrValue : 16,
					value : "#f69679"
				}, {
					attrValue : 17,
					value : "#c7b299"
				}, {
					attrValue : 18,
					value : "#c69c6d"
				}, {
					attrValue : 19,
					value : "#998675"
				}, {
					attrValue : 20,
					value : "#a67c52"
				}, {
					attrValue : 21,
					value : "#cdcdcd"
				}, {
					attrValue : 22,
					value : "#b4b4b4"
				}]
			}
		},
		compoundLabelFontSize : 96, // 64, temp change
		labelFontSize : 36, //36,
		//labelFontWeight : "bold",
		compoundLabelFontWeight : "bold",
		compoundLabelVerticalAnchor : "bottom",
		compoundLabelYOffset : 35,
		compoundLabelFontColor : "#666666",
		labelHorizontalAnchor : "center"
	},
	edges : {
		width : {
			 passthroughMapper: { attrName: "mappervalue" } 
		},
		color : "#df0029"
	}
};
