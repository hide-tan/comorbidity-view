<?php
// check if the data are sent by post method with variables "dcode", "gender", "byear", and "threshold" 
if (isset($_POST["dcode"], $_POST["gender"], $_POST["byear"], $_POST["threshold"])) {
	// set path to chapter data
	$chapter_content = "/usr/local/bib/networkdata/chapter";
	
	//check if the dcode data is a comorbidity pair (matches reg exp. "xxx-xxx")
	if (ereg("...-...", $_POST["dcode"])) {
		// put diagnosis codes of a comorbidity pair into an array (delimiter "-")
		$dcode_arr = explode("-", $_POST["dcode"]);
		// set path to one diagnosis data
		$node_content_source = normalize("/usr/local/bib/networkdata/each/node/" . $dcode_arr[0] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
		$edge_content_source = normalize("/usr/local/bib/networkdata/each/edge/" . $dcode_arr[0] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
		// set path to another diagnosis data
		$node_content_target = normalize("/usr/local/bib/networkdata/each/node/" . $dcode_arr[1] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
		$edge_content_target = normalize("/usr/local/bib/networkdata/each/edge/" . $dcode_arr[1] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
		
		// check if the diagnosis data exist
		if (file_exists($chapter_content) && file_exists($node_content_source) && file_exists($edge_content_source) && file_exists($node_content_target) && file_exists($edge_content_target)) {
			// if exists, print a network data
			print "{nodes: [" . trim(file_get_contents($chapter_content)) . "," . trim(file_get_contents($node_content_source)) . "," . trim(file_get_contents($node_content_target)) . "], edges: [" . trim(file_get_contents($edge_content_source)) . "," . trim(file_get_contents($edge_content_target)) . "]}";
		} else
			// otherwise print an error message
			print "error";
	} else {
		$node_content = "";
		$edge_content = "";
		// check if the dcode data is "all" 
		if (strnatcasecmp($_POST["dcode"], "all") == 0) { 
			$node_content = normalize("/usr/local/bib/networkdata/" . $_POST["threshold"] . "/node/" . $_POST["dcode"] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
			$edge_content = normalize("/usr/local/bib/networkdata/" . $_POST["threshold"] . "/edge/" . $_POST["dcode"] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
		} 
		// or a selected diagnosis code
		else {
			$node_content = normalize("/usr/local/bib/networkdata/each/node/" . $_POST["dcode"] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
			$edge_content = normalize("/usr/local/bib/networkdata/each/edge/" . $_POST["dcode"] . "-" . $_POST["gender"] . "-" . $_POST["byear"]);
		}
		
		// check if the diagnosis data exist
		if (file_exists($chapter_content) && file_exists($node_content) && file_exists($edge_content)) {
			// if exist, print a network data
			print "{nodes: [" . trim(file_get_contents($chapter_content)) . "," . trim(file_get_contents($node_content)) . "], edges: [" . trim(file_get_contents($edge_content)) . "]}";
		} else
			// otherwise print an error message
			print $node_content." error";
	}
}

// remove string "", "." and ".." from path
function normalize ($input){
	$component = explode("/", $input);
	$npath = "";
	foreach ($component as $value){
		if (strnatcasecmp($value, "") != 0 and strnatcasecmp($value, ".") != 0 and strnatcasecmp($value, "..") != 0){
			$npath .= "/".$value;
		}
	}
	return $npath;
}
?>
