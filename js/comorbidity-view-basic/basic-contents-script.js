$(function() {
	var cytoscape_param = $.data($("#cytoscapeweb").get(0));

	// Tabs
	var loaded = new Boolean(false);
	var layout_data = "";
	$('#tabs').tabs({
		select : function(event, ui) {
			if(ui.index == 1 && loaded == false) {
				loaded = true;

				// php-script for fetching network data
				var networkdata_request = "php/networkdata_request.php";
				// php-script for fetching filter value for the searching dcode
				var search_request = "php/search_request.php";

				// setting current view html tags
				$("#view").html('<div id="current_view"><table cellpadding="4" width="100%"><tr><td rowspan="3" width="25%"><div class="info_head_big">Current View</div></td><td  width="35%">Top <span id="current_threshold_common" class="view_status"> </span> most common comorbidity pairs</td><td  width="40%">Number of patients of comorbidity pairs: <span id="current_min" class="min_range"> </span> - <span id="current_max" class="max_range"> </span> </td></tr><tr><td>Gender: <span id="current_gender" class="view_status"> </span> </b>patients</td><td>Birth year group: <span id="current_byear" class="view_status"> </span> </td></tr><tr><td>Description language mode: <span id="current_language_mode" class="view_status"> </span> </td><td>Selected diagnosis code: <span id="current_selected_dcode" class="view_status"> </span> </td></tr></table></div><div id="cytoscapeweb"></div><div id="error_msg" class="info"></div>');

				// default value for radio button & check box
				$("input[name='language_mode']").val(['Swedish']);
				$("input[name='threshold']").val(['100']);
				$("input[name='g_filter']").val(['all']);
				$("input[name='byear_group']").val(['all']);

				// initialization of tool
				var vis = new org.cytoscapeweb.Visualization(div_id, options);
				layout_data = {
					name : "CompoundSpringEmbedder",
					options : default_layout_options
				};

				// draw the default view
				switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, "all", "all", "all", "100");

				// set values for default view
				$.data(cytoscape_param, "language", "Swedish");
				$.data(cytoscape_param, "dcode", "all");
				$.data(cytoscape_param, "gender", "all");
				$.data(cytoscape_param, "byear", "all");
				$.data(cytoscape_param, "threshold", "100");
				// possible max number of comorbidity pairs
				$.data(cytoscape_param, "min_value", "0");
				$.data(cytoscape_param, "max_value", "10643");

				// add a listener for when nodes and edges are clicked
				vis.addListener("click", "nodes", function(event) {
					handle_click_node(event);
				}).addListener("click", "edges", function(event) {
					handle_click_edge(event);
				}).addListener("error", function(event) {
					$("#error_msg").dialog({
						title : "Error"
					});
					$("#error_msg").html("No network data exists within the selected threshold for:");
					$("#error_msg").append('<ul><li>Diagnosis code: <b>' + $.data(cytoscape_param, "dcode") + '<\/b><\/li><li>Gender: <b>' + $.data(cytoscape_param, "gender") + '<\/b><\/li><li>Birth year group: <b>' + $.data(cytoscape_param, "byear") + '<\/b><\/li><\/ul>');
					$.data(cytoscape_param, "min_value", "");
					$.data(cytoscape_param, "max_value", "");
					$("#current_threshold_common").html($.data(cytoscape_param, "threshold"));
					$("#current_selected_dcode").html($.data(cytoscape_param, "dcode"));
					$("#current_gender").html($.data(cytoscape_param, "gender"));
					$("#current_byear").html($.data(cytoscape_param, "byear"));
					$(".min_range").html("");
					$(".max_range").html("");
					$("#min_comorb").html("");
					$("#max_comorb").html("");
					$("#error_msg").dialog("open");
					$("#cytoscapeweb").html("<img src='img/Startup_small.jpg' alt=''>");
				});
				
				// set the property value of the selected (clicked) node (except chapters)
				function handle_click_node(event) {
					var target = event.target;

					if(target.data["id"] > 22) {
						$("#view_function").accordion("activate", 1);
						$("#property_field").append('<p><big><b>Code<\/b>: ' + target.data["label"] + '<\/big><\/p>');
						if($.data(cytoscape_param, "language") == "Swedish")
							$("#property_field").append("<p><b>Description<\/b>: " + target.data["swe_description"] + "<\/p>");
						else
							$("#property_field").append("<p><b>Description<\/b>: " + target.data["eng_description"] + "<\/p>");
						$("#property_field").append('<p><b>Number of ' + target.data["label"] + ' patients<\/b>: ' + target.data["frequency"] + ' (Gender: ' + $.data(cytoscape_param, "gender") + ', Birth year: ' + $.data(cytoscape_param, "byear") + ')<\/p>');

						$("#property_field").append('<hr color="#ccc">');
						// move to the latest inserted property data
						if($("#property_field").length == 0)
							return;
						$("#property_field").scrollTop($("#property_field")[0].scrollHeight);
					}
				}
				
				// set the property value of the selected (clicked) edge
				function handle_click_edge(event) {
					var target = event.target;
					$("#view_function").accordion("activate", 1);
					$("#property_field").append("<p><big><b>Comorbidity pair<\/b>: " + target.data["source_code"] + " and " + target.data["target_code"] + "<\/big><\/p>");
					$("#property_field").append('<p><b>Number of patients with this comorbidity pair<\/b>: ' + target.data["frequency"] + ' (Gender: ' + $.data(cytoscape_param, "gender") + ', Birth year: ' + $.data(cytoscape_param, "byear") + ')<\/p>');
					$("#property_field").append('<p><ul><li>' + target.data["source_percentage"] + ' % of ' + target.data["source_code"] + ' patients<\/li><li> ' + target.data["target_percentage"] + ' % of ' + target.data["target_code"] + ' patients<\/li><\/ul><\/p>');
					$("#property_field").append('<hr color="#ccc">');
					// move to the latest inserted property data
					if($("#property_field").length == 0)
						return;
					$("#property_field").scrollTop($("#property_field")[0].scrollHeight);
				}

				// callback when Cytoscape Web has finished drawing
				vis.ready(function() {
					
					// set min value for a new view
					var min = vis.edges().sort(sortComorbFreq).shift().data["frequency"];
					// set min value for a previous view
					var min_value = eval($.data(cytoscape_param, "min_value"));
					// set max value for a new view
					var max = vis.edges().sort(sortComorbFreq).pop().data["frequency"];
					// set max value for a previous view
					var max_value = eval($.data(cytoscape_param, "max_value"));
					
					// set slider bar for "number of patients of comorbidity pairs"
					$('#comorbfilter').slider({
						range : true,
						values : [0, 0],
						min : min,
						max : max,
						slide : function(event, ui) {
							$(".min_range").html(ui.values[0]);
							$(".max_range").html(ui.values[1]);
							$.data(cytoscape_param, "min_value", ui.values[0]);
							$.data(cytoscape_param, "max_value", ui.values[1]);
							filterRange($.data(cytoscape_param, "current_checked_chapters"));
							vis.zoomToFit();
						}
					});
					
					// set min and max value for slider
					$("#comorbfilter").slider("values", 0, min);
					$("#comorbfilter").slider("values", 1, max);
					
					// set filter value for the current view
					$("#current_language_mode").html($.data(cytoscape_param, "language"));
					$("#current_threshold_common").html($.data(cytoscape_param, "threshold"));
					$("#current_selected_dcode").html($.data(cytoscape_param, "dcode"));
					$("#current_gender").html($.data(cytoscape_param, "gender"));
					$("#current_byear").html($.data(cytoscape_param, "byear"));
					
					// set min and max values
					$(".min_range").html(min);
					$(".max_range").html(max);

					$.data(cytoscape_param, "min_value", min);
					$.data(cytoscape_param, "max_value", max);

					$("#filterrange").html('<tr><td id="min_comorb" align="left"></td><td align="center"><span class="min_range">' + $.data(cytoscape_param, "min_value") + '</span> - <span class="max_range">' + $.data(cytoscape_param, "max_value") + '</span></td><td id="max_comorb" align="right"></td></tr>');

					$("#min_comorb").html("<b>" + $.data(cytoscape_param, "min_value") + "<\/b>");
					$("#max_comorb").html("<b>" + $.data(cytoscape_param, "max_value") + "<\/b>");
					
					// remove chapter nodes which do not contain any diagnosis codes
					filterChapterNoChild();
					
					// put checked chapters into an array "checkedChapters"
					var checkedChapters = new Array;
					$("#chapter_filter input:checkbox:checked").map(function() {
						checkedChapters.push($(this).val());
					});

					$.data(cytoscape_param, "default_checked_chapters", checkedChapters);
					$.data(cytoscape_param, "current_checked_chapters", checkedChapters);
					
					// filter out the diagnosis codes outside the range
					filterRange($.data(cytoscape_param, "current_checked_chapters"));
					
					// set the description language
					changeLanguageMode($.data(cytoscape_param, "language"));
					
					// fit the view to the window
					vis.zoomToFit();
				});
				
				// add a lister for enter key
				$("#find_dcode").keypress(function(event) {
					if(event.which == 13) {
						findDcode();
					}
				});
				
				// add a lister for "Find" button
				$("#find_btn").click(function() {
					findDcode();
				});
				
				// find a node (diagnosis code) in a current view
				function findDcode() {
					var found = new Boolean(false);
					var find_dcode = $("#find_dcode").val().toLowerCase();
					// check if the entered code matches roughly level3 ICD-10 code (reg exp [a-z][0-9][0-9])
					if(find_dcode.match(/[a-z][0-9][0-9]/)) {
						vis.deselect("nodes");
						// if the entered code exists in the current view, select the node
						for(i in vis.nodes()) {
							if(vis.nodes()[i].data["label"] == find_dcode) {
								var n = vis.nodes()[i];
								vis.select([n]);
								found = true;
								break;
							}
						}
						
						// otherwise show an error dialog message
						if(found == false) {
							$("#find_error_info").html("Not found " + find_dcode + " in the current view.");
							$("#find_error_info").dialog({
								title : "Error"
							});
							$("#find_error_info").dialog("open");
						}
					} 
					// if the entered code does not match reg exp [a-z][0-9][0-9], then show an error dialog message
					else {
						showCodeMatchError();
					}
				}
				
				// add a lister for language selection
				$("#language_selection input:radio").click(function() {
					var sel_language = $("input[name=language_mode]:checked").val();
					$.data(cytoscape_param, "language", sel_language);

					$("#property_field").html("");
					changeLanguageMode(sel_language);
				});
				
				// switch description language between English and Swedish 
				function changeLanguageMode(language) {
					for(i in vis.nodes().sort(sortDcode)) {
						if(vis.nodes().sort(sortDcode)[i].data["id"] < 23) {
							$("#chapter" + vis.nodes().sort(sortDcode)[i].data["id"]).html(" (" + vis.nodes().sort(sortDcode)[i].data["frequency"] + " patients): ");
							if(language == "Swedish") {
								$("#chapter" + vis.nodes().sort(sortDcode)[i].data["id"]).append(vis.nodes().sort(sortDcode)[i].data["swe_description"]);
								$.data(cytoscape_param, "language", "Swedish");
								$("#current_language_mode").html($.data(cytoscape_param, "language"));
							} else {
								$("#chapter" + vis.nodes().sort(sortDcode)[i].data["id"]).append(vis.nodes().sort(sortDcode)[i].data["eng_description"]);
								$.data(cytoscape_param, "language", "English");
								$("#current_language_mode").html($.data(cytoscape_param, "language"));
							}
						} else
							break;
					}
				}

				// Select most common comorbidity pairs
				$("#com_comorb_setting input:radio").click(function() {
					var newthreshold = $("input[name=threshold]:checked").val();
					switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, $.data(cytoscape_param, "dcode"), $.data(cytoscape_param, "gender"), $.data(cytoscape_param, "byear"), newthreshold);
					$.data(cytoscape_param, "threshold", newthreshold);
				});
				// Filter out unchecked chapter
				$("#chapter_filter input:checkbox").click(function() {

					var checkedChapters = new Array;
					$("#chapter_filter input:checkbox:checked").map(function() {
						checkedChapters.push($(this).val());
					});
					$.data(cytoscape_param, "current_checked_chapters", checkedChapters);

					filterRange($.data(cytoscape_param, "current_checked_chapters"));
					vis.zoomToFit();
				});
				
				// filter out chapters which do not contain any diagnosis codes
				function filterChapterNoChild() {
					var chapterArray = new Array();
					for( i = 1; i < 23; i++) {
						if(vis.childNodes(i).length > 0)
							chapterArray.push(i);
					}
					$("input[name = 'chapter']").val(chapterArray);
				}
				
				// filter out the diagnosis codes outside the range
				function filterRange(checkedChapters) {
					var rangeNode = new Array();
					vis.filter("edges", function(edge) {
						if(edge.data.frequency >= $.data(cytoscape_param, "min_value") && edge.data.frequency <= $.data(cytoscape_param, "max_value")) {
							rangeNode.push(edge.data["source"]);
							rangeNode.push(edge.data["target"]);
						}
						return edge.data.frequency >= $.data(cytoscape_param, "min_value") && edge.data.frequency <= $.data(cytoscape_param, "max_value");
					}, true);
					var rangeChapter = new Array();

					vis.filter("nodes", function(node) {
						if(node.data.id > 22 && rangeNode.indexOf(node.data.id) != -1)
							rangeChapter.push(node.data["parent"]);
						return;
					}, true);
					var uniqueRangeChapter = unique(rangeChapter);
					vis.filter("nodes", function(node) {
						return (((node.data.id < 23 && uniqueRangeChapter.indexOf(node.data.id) != -1) || (node.data.id > 22 && rangeNode.indexOf(node.data.id) != -1)) && (checkedChapters.indexOf(node.data.id) != -1 || node.data.id > 22));
					}, true);
					for( i = 1; i < 23; i++) {
						var istring = i + "";
						if(uniqueRangeChapter.indexOf(istring) != -1)
							document.chapter_filter.chapter[i - 1].disabled = false;
						else
							document.chapter_filter.chapter[i - 1].disabled = true;
					}
					$("input[name = 'chapter']").val(checkedChapters);
				}
				
				// uncheck all check boxes for chapter selection
				$("#uncheck_all_chapter").click(function() {
					$('#chapter_filter input').removeAttr('checked');
					vis.filter("nodes", []);
				});
				
				// reset check boxes to the default values
				$("#reset_chapter_filter").click(function() {
					filterRange($.data(cytoscape_param, "default_checked_chapters"));
					vis.zoomToFit();
				});
				
				// switch view from a selected diagnosis code to all diagnosis code
				$("#back_overall").click(function() {
					switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, "all", $.data(cytoscape_param, "gender"), $.data(cytoscape_param, "byear"), $.data(cytoscape_param, "threshold"));
					$.data(cytoscape_param, "dcode", "all");
				});
				
				// clear property field
				$("#clear_property").click(function() {
					$("#property_field").html("");
				});
				
				// Change gender view
				$("#gender_selection input:radio").click(function() {
					var gender = $("input[name=g_filter]:checked").val();
					switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, $.data(cytoscape_param, "dcode"), gender, $.data(cytoscape_param, "byear"), $.data(cytoscape_param, "threshold"));
					$.data(cytoscape_param, "gender", gender);

				});
				// Select birth year group
				$("#byear_group_selection input:radio").change(function() {
					var byear = $("input[name=byear_group]:checked").val();
					switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, $.data(cytoscape_param, "dcode"), $.data(cytoscape_param, "gender"), byear, $.data(cytoscape_param, "threshold"));
					$.data(cytoscape_param, "byear", byear);
				});
				
				// show info dialog for current view
				$("#view_head").click(function() {
					$("#view_info").dialog({
						title : "Current view"
					});
					$("#view_info").dialog("open");
				});
				
				// show info dialog for description language
				$("#language_mode_head").click(function() {
					$("#language_mode_info").dialog({
						title : "Description language"
					});
					$("#language_mode_info").dialog("open");
				});
				
				// show info dialog for property
				$("#property_head").click(function() {
					$("#property_info").dialog({
						title : "Property"
					});
					$("#property_info").dialog("open");
				});
				
				// show info dialog for threshold for comorbidity pairs
				$("#threshold_head").click(function() {
					$("#threshold_info").dialog({
						title : "Threshold for comorbidity pairs"
					});
					$("#threshold_info").dialog("open");
				});
				
				// show info dialog for chapter selection
				$("#chapter_head").click(function() {
					$("#chapter_info").dialog({
						title : "Chapter (ICD-10) selection"
					});
					$("#chapter_info").dialog("open");
				});
				
				// show info dialog for gender selection
				$("#gender_head").click(function() {
					$("#gender_info").dialog({
						title : "Gender"
					});
					$("#gender_info").dialog("open");
				});
				
				// show info dialog for birth year group selection
				$("#byear_head").click(function() {
					$("#byear_info").dialog({
						title : "Birth year group"
					});
					$("#byear_info").dialog("open");
				});
				
				// show info dialog for find a node
				$("#find_head").click(function() {
					$("#find_info").dialog({
						title : "Find a node (diagnosis code) in a current view"
					});
					$("#find_info").dialog("open");
				});
				
				// show info dialog for network view for a selected item
				$("#network_selected_head").click(function() {
					$("#network_selected_info").dialog({
						title : "Network view for a selected item"
					});
					$("#network_selected_info").dialog("open");
				});
				
				// show info dialog for search function
				$("#search_head").click(function() {
					$("#search_info").dialog({
						title : "Search in which network view a diagnosis code appears"
					});
					$("#search_info").dialog("open");
				});
				
				// show info dialog for save function
				$("#save_image_head").click(function() {
					$("#save_image_info").dialog({
						title : "Save view as"
					});
					$("#save_image_info").dialog("open");
				});
				
				// add a listener for enter key
				$("#network_seldcode").keypress(function(event) {
					if(event.which == 13) {
						showNetworkSelcode();
					}
				});
				
				// add a lisner for show button (diagnosis code)
				$("#show_selcode_btn").click(function() {
					showNetworkSelcode();
				});
				
				// show network view for an entered diagnosis code
				function showNetworkSelcode() {
					var dcode = $("#network_seldcode").val().toLowerCase();
					// check if the entered code matches roughly ICD-10 level3 code (reg exp [a-z][0-9][0-9])
					if(dcode.match(/[a-z][0-9][0-9]/)) {
						switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, dcode, $.data(cytoscape_param, "gender"), $.data(cytoscape_param, "byear"), $.data(cytoscape_param, "threshold"));
						$.data(cytoscape_param, "dcode", dcode);
					} 
					// if the entered code does not match reg exp, show an error dialog message
					else {
						showCodeMatchError();
					}
				}
				
				// add a listener for enter key
				$(".selpair").keypress(function(event) {
					if(event.which == 13) {
						showNetworkSelpair();
					}
				});
				
				// add a listener for show button (comorbidity pair)
				$("#show_selpair_btn").click(function() {
					showNetworkSelpair();
				});
				
				// show network view for an entered comorbidity pair (dcode-dcode)
				function showNetworkSelpair() {
					var dcode = $("#network_selpair_1").val().toLowerCase() + "-" + $("#network_selpair_2").val().toLowerCase();
					// check if the entered code matches roughly ICD-10 level3 code (reg exp [a-z][0-9][0-9])
					if(dcode.match(/[a-z][0-9][0-9]-[a-z][0-9][0-9]/)) {
						switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, dcode, $.data(cytoscape_param, "gender"), $.data(cytoscape_param, "byear"), $.data(cytoscape_param, "threshold"));
						$.data(cytoscape_param, "dcode", dcode);
					} 
					// if the entered code does not match reg exp, show an error dialog message
					else {
						showCodeMatchError();
					}
				}
				
				// add a listener for enter key
				$("#search_dcode").keypress(function(event) {
					if(event.which == 13) {
						search();
					}
				});
				
				// add a listener for search button
				$("#search_btn").click(function() {
					search();
				});
				
				// show a list of filter values (gender, byear, threshold) for an entered diagnosis code
				function search() {
					var search_dcode = $("#search_dcode").val();
					// check if the entered code matches roughly ICD-10 level3 code (reg exp [a-z][0-9][0-9])
					if(search_dcode.match(/[a-z][0-9][0-9]/)) {
						$("#search_result").html("");
						searchDcode(search_request, search_dcode);
					} 
					// if the entered code does not match reg exp, show an error dialog message
					else {
						showCodeMatchError();
					}
				}
				
				// show an error dialog message when an entered diagnosis code does not match reg exp [a-z][0-9][0-9]
				function showCodeMatchError() {
					$("#code_match_error").html("Entered code does not match ICD-10 code.");
					$("#code_match_error").dialog({
						title : "Invalid code"
					});
					$("#code_match_error").dialog("open");
				}
				
				// show a network view for selected filter values from search result
				$(".network_from_table").live("click", function() {
					// the value of name attribute contains each parameter for gender, birth year and threshold combined with delimiter "#"
					// (see search_request.php)
					// each parameter is put into array "param"
					var param = $(this).attr("name").split("#");
					switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, "all", param[0], param[1], param[2]);
					$.data(cytoscape_param, "gender", param[0]);
					$.data(cytoscape_param, "byear", param[1]);
					$.data(cytoscape_param, "threshold", param[2]);
					$("input[name='g_filter']").val([param[0]]);
					$("input[name='byear_group']").val([param[1]]);
					$("input[name='threshold']").val([param[2]]);
					$("#current_threshold_common").html($.data(cytoscape_param, "threshold"));
					$("#current_gender").html($.data(cytoscape_param, "gender"));
					$("#current_byear").html($.data(cytoscape_param, "byear"));
				});
				
				// save the current view as a image
				$("#save_to_file").click(function() {
					var format = $("input[name=file_format]:checked").val();
					// select image format
					if(format == "png")
						vis.exportNetwork("png", "php/export_view.php?type=png");
					else
						vis.exportNetwork("svg", "php/export_view.php?type=svg");
				});
				
				// set a dialog window
				$(".info").dialog({
					autoOpen : false,
					closeOnEscape : false,
					modal : true,
					width : 480,
					buttons : {
						"OK" : function() {
							$(this).dialog('close');
						}
					}
				});
				
				// reset to the default view
				$("#reset_layout").click(function() {
					$.data(cytoscape_param, "dcode", "all");
					$.data(cytoscape_param, "gender", "all");
					$("input[name='g_filter']").val(['all']);
					$.data(cytoscape_param, "byear", "all");
					$("input[name='byear_group']").val(['all']);
					$.data(cytoscape_param, "threshold", "100");
					$("input[name='threshold']").val(['100']);

					// possible max number of comorbidity pairs
					$.data(cytoscape_param, "min_value", "0");
					$.data(cytoscape_param, "max_value", "10643");
					switchView(vis, networkdata_request, comorb_data_schema, layout_data, visual_style_data, "all", "all", "all", "100");
				});
			}
		}
	});

	// sharpen the corners of tab windows
	$('#tabs .ui-tabs-nav').removeClass('ui-corner-all');

	// hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(function() {
		$(this).addClass('ui-state-hover');
	}, function() {
		$(this).removeClass('ui-state-hover');
	});
	
	// set accordions for menus
	$("#view_function").accordion({
		header : "h2",
		active : false,
		collapsible : false, //true, change to "true" if all accordions allow to be closed
		autoHeight : false,
		icons : {
			'header' : 'ui-icon-plus',
			'headerSelected' : 'ui-icon-minus'
		}
	});
	
	// sort diagnosis codes in a current view in ascending order
	function sortDcode(a, b) {
		return (a.data["label"] > b.data["label"]) ? 1 : -1;
	}
	
	// sort diagnosis codes' frequency in a current view  in ascending order
	function sortComorbFreq(a, b) {
		return (a.data["frequency"] > b.data["frequency"]) ? 1 : -1;
	}
	
	// remove duplications in an array
	function unique(array) {
		var storage = {};
		var uniqueArray = [];
		var i, value;
		for( i = 0; i < array.length; i++) {
			value = array[i];
			if(!( value in storage)) {
				storage[value] = true;
				uniqueArray.push(value);
			}
		}
		return uniqueArray;
	}
	
	// switch network view
	function switchView(vis, url, dataschema, layout, visualstyle, dcode, gender, byear, threshold) {
		var networkdata = "";
		$.ajax({
			async : false,
			url : url,
			type : "post",
			data : "dcode=" + dcode + "&gender=" + gender + "&byear=" + byear + "&threshold=" + threshold,
			success : function(data) {
				if(!data.match("error")) {
					// convert data into javascript object
					networkdata = (new Function("return " + data))();
				}
			}
		});
		var vis_data = {
			dataSchema : dataschema,
			data : networkdata
		};
		vis.draw({
			network : vis_data,
			layout : layout_data,
			visualStyle : visual_style_data
		});
	}
	
	// search filter values (gender, byear and threshold) for a diagnosis code
	function searchDcode(url, dcode) {
		$.ajax({
			async : false,
			url : url,
			type : "post",
			data : "dcode=" + dcode,
			beforeSend : function(XHRoj) {
				$("#search_result").html('<span id="searching">Searching ...</span>');
			},
			success : function(data) {
				if(!data.match("error")) {
					$("#search_result").html(data);
				}
			}
		});
	}

});
