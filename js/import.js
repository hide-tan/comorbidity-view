// Template
// document.write('<script type="text/javascript" src=""></script>');

// JSON support for IE (needed to use JS API)
document
		.write('<script type="text/javascript" src="./js/min/json2.min.js"></script>');
// Flash embedding utility (needed to embed Cytoscape Web)
document
		.write('<script type="text/javascript" src="./js/min/AC_OETags.min.js"></script>');
// Cytoscape Web JS API (needed to reference org.cytoscapeweb.Visualization)
document
		.write('<script type="text/javascript" src="./js/min/cytoscapeweb.min.js"></script>');
// Jquery
document
		.write('<script type="text/javascript" src="./js/min/jquery-1.6.4.min.js"></script>');
// document.write('<script type="text/javascript"
// src="./js/src/jquery.cookie.js"></script>'); // Jquery cookie
// Jquery ui
document
		.write('<script type="text/javascript" src="./js/min/jquery-ui-1.8.16.custom.min.js"></script>');
// Basic visual preference for Comorbidity-View
document
		.write('<script type="text/javascript" src="./js/comorbidity-view-basic/basic-visual-preference.js"></script>');
// Basic contents preference for Comorbidity-View
document
		.write('<script type="text/javascript" src="./js/comorbidity-view-basic/basic-contents-script.js"></script>');
// Data schema for Comorbidity-View
document
		.write('<script type="text/javascript" src="./js/comorbidity-view-basic/comorbidity-view-data-schema.js"></script>');
